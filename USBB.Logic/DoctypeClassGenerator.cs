﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USBB.Model;
using USBB.Domain.Logic;

namespace USBB.Logic
{
    public class DoctypeClassGenerator : IDoctypeClassGenerator
    {
        public string Generate(DocumentType model)
        {
            if(model == null)
            {
                return string.Empty;
            }

            var classTemplate = "using Vega.USiteBuilder;\n\nnamespace {0}\n{{\n    {1}\n    public class {2} : {3}\n    {{\n{4}    }}\n}}";
            var propertyTemplate = "        {0}\n        public string {1} {{ get; set; }}\n\n";

            var builder = new StringBuilder();
            foreach(var property in model.Properties)
            {
                builder.AppendFormat(propertyTemplate, CreatePropertyAttribute(property), property.Identifier);
            }

            if(builder.Length < 1)
            {
                return string.Format(classTemplate, model.Namespace, CreateClassAttribute(model), model.Identifier, model.ParentDocumentType == null ? "DocumentTypeBase" : model.ParentDocumentType.Identifier, string.Empty);
            }

            return string.Format(classTemplate, model.Namespace, CreateClassAttribute(model), model.Identifier, model.ParentDocumentType == null ? "DocumentTypeBase" : model.ParentDocumentType.Identifier, builder.ToString().Substring(0, builder.Length - 1));
        }

        private string CreatePropertyAttribute(DocumentTypeProperty model)
        {
            var attrBase = "[DocumentTypeProperty({0})]";
            var builder = new StringBuilder();
            builder.Append("UmbracoPropertyType." + model.UmbracoPropertyType);
            if(model.Alias != null)
            {
                builder.Append(", Alias = \"" + model.Alias + "\"");
            }

            if (model.DefaultValue != null)
            {
                builder.Append(", DefaultValue = \"" + model.DefaultValue + "\"");
            }

            if (model.Description != null)
            {
                builder.Append(", Description = \"" + model.Description + "\"");
            }

            if (model.Mandatory)
            {
                builder.Append(", Mandatory = true");
            }

            if (model.Name != null)
            {
                builder.Append(", Name = \"" + model.Name + "\"");
            }

            if (model.OtherTypeName != null)
            {
                builder.Append(", OtherTypeName = \"" + model.OtherTypeName + "\"");
            }

            if (model.Tab != null)
            {
                builder.Append(", Tab = \"" + model.Tab + "\"");
            }

            if (model.ValidationRegExp != null)
            {
                builder.Append(", ValidationRegExp = \"" + model.ValidationRegExp + "\"");
            }

            return string.Format(attrBase, builder.ToString());
        }

        private string CreateClassAttribute(DocumentType model)
        {
            var attrBase = "[DocumentType({0})]";
            var builder = new StringBuilder();
            builder.Append("Alias = \"" + model.Alias + "\"");

            if (model.AllowedChildNodeTypes.Any())
            {
                builder.Append(", AllowedChildNodeTypes = new [] { typeof(" + string.Join("), typeof(", model.AllowedChildNodeTypes) + ") }");
            }

            if (model.AllowedTemplates.Any())
            {
                builder.Append(", AllowedTemplates = new [] { \"" + string.Join("\", \"", model.AllowedTemplates) + "\" }");
            }

            if(model.DefaultTemplate != null)
            {
                builder.Append(", DefaultTemplate = \"" + model.DefaultTemplate + "\"");
            }

            if (model.Description != null)
            {
                builder.Append(", Description = \"" + model.Description + "\"");
            }

            if (model.IconUrl != null)
            {
                builder.Append(", IconUrl = \"" + model.IconUrl + "\"");
            }

            if (model.Name != null)
            {
                builder.Append(", Name = \"" + model.Name + "\"");
            }

            if (model.Thumbnail != null)
            {
                builder.Append(", Thumbnail = \"" + model.Thumbnail + "\"");
            }

            if(model.AllowAtRoot)
            {
                builder.Append(", AllowAtRoot = true");
            }

            return string.Format(attrBase, builder.ToString());
        }
    }
}
